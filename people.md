---
title: People
layout: default
permalink: /people/
---

### Postdocs
- [Gunes Baydin](http://www.robots.ox.ac.uk/~gunes/)

### PhD students
- [Bradley Gram-Hansen](http://www.robots.ox.ac.uk/~bradley)
- [Michael Teng]()
- [Rob Zinkov](http://www.zinkov.com/)
- [Yuan Zhou](https://www.cs.ox.ac.uk/people/yuan.zhou/)
- [Tuan Anh Le](http://www.tuananhle.co.uk/)
- [Stefan Webb](http://stefanwebb.uk)
- [Andrew Warrington](http://www.robots.ox.ac.uk/~andreww)
- [Max Igl](http://maximilianigl.github.io/)
- [Rob Cornish](http://www.robots.ox.ac.uk/~rcornish/index.html)

<!-- ### MS students -->
### Alumni

- [Tobias Kohn]() -> Cambridge PostDoc
- [Marcin Szymczak](https://www.inf.ed.ac.uk/people/students/Marcin_Szymczak.html) -> Oxford PostDoc
- [Tom Rainforth](http://www.robots.ox.ac.uk/~twgr) -> Oxford PostDoc
- [Brooks Paige](http://www.robots.ox.ac.uk/~brooks) -> Alan Turing Institute PostDoc
- [Jan Willem van de Meent](http://www.robots.ox.ac.uk/~jwvdm/) -> Northeastern Faculty
- [David Tolpin](http://offtopia.net/) -> Paypal Research
- [Jonathan Huggins](http://www.jhhuggins.org/) -> MIT PhD
- [Willie Neiswanger](http://www.cs.cmu.edu/~wdn/) -> CMU PhD
- Carl Smith -> Moat
- [David Pfau](http://davidpfau.com/) -> DeepMind
- [Jan Gasthaus](http://www.gatsby.ucl.ac.uk/~ucabjga/) -> Amazon
- Nicholas Bartlett -> [Nicholas Campelia](https://www.linkedin.com/in/nicholas-campelia-4695a29) -> Two Sigma
- [Yura Perov](http://www.robots.ox.ac.uk/~perov/) -> Babylon Health
- [Tom Jin](https://www.stats.ox.ac.uk/~sjin/) -> Merrill Lynch
- [Neil Dhir](http://www.robots.ox.ac.uk/~neild/)

### Collaborators

- [Hongseok Yang](http://www.cs.ox.ac.uk/people/hongseok.yang/Public/Home.html)
- [Yee Whye Teh](http://www.stats.ox.ac.uk/~teh/)
- [Arnaud Doucet](http://www.stats.ox.ac.uk/~doucet/)
- [Vikash Mansingkha](http://web.mit.edu/vkm/www/)
- [Andy Gordon](https://onedrive.live.com/view.aspx/adg?cid=c6149b019d236bf5)
- [Jeffrey Siskind](https://engineering.purdue.edu/~qobi/)
